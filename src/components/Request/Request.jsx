import "./Request.css";
import ReactDOM from "react-dom";

import { GoX, GoCheck } from "react-icons/go";
import RequestDetails from "../RequestDetails/RequestDetails";
import {
  SwipeableList,
  SwipeableListItem,
} from "@sandstreamdev/react-swipeable-list";
import "@sandstreamdev/react-swipeable-list/dist/styles.css";
import { useState } from "react";

const Request = (props) => {
  let ob_id = props.index + "-request";
  let red_color = "rgb(242 98 87)";
  let green_color = "rgb(64 179 99)";
  let first_color;
  if (props.request.is_authurized === "") {
    first_color = "rgb(103 123 130)";
  } else if (props.request.is_authurized === "true") {
    first_color = green_color;
  } else {
    first_color = red_color;
  }

  const openDetails = () => {
    let r_loc =
      document.getElementById(ob_id).offsetParent.offsetParent.offsetTop +
      document.getElementById(ob_id).offsetHeight +
      1;

    ReactDOM.render(
      <RequestDetails
        loc={r_loc}
        request={props.request}
        index={props.index}
      />,
      document.getElementById("details")
    );
  };

  const handleSwiping = (progress) => {
    if (progress > 30) {
      document.getElementById(ob_id).style.border = "3px solid gray";
    } else {
      document.getElementById(ob_id).style.border = "none";
    }
  };
  const setAuth = (auth) => {
    let req =
      window.serverUrl +
      "?command=" +
      auth +
      "&timeStamp=" +
      props.request.timeStamp;
    console.log(req);

    fetch(req)
      .then((response) => response.json())
      .then((data) => console.log(data))
      .catch((error) => {});

    let new_color = auth === "enable" ? green_color : red_color;
    document.getElementById(ob_id).style.background = new_color;
    document.getElementById(ob_id).style.border = "none";
  };

  return (
    <SwipeableListItem
      swipeLeft={{
        content: (
          <div className="onLeft swipe-back" style={{ background: red_color }}>
            <GoX style={{ width: "3em", height: "100%" }} />
          </div>
        ),
        action: () => {
          setAuth("disable");
        },
      }}
      swipeRight={{
        content: (
          <div
            className="onRight swipe-back"
            style={{ background: green_color }}
          >
            <GoCheck style={{ width: "3em", height: "100%" }} />
          </div>
        ),
        action: () => {
          setAuth("enable");
        },
      }}
      onSwipeProgress={(progress) => handleSwiping(progress)}
      threshold={0.3}
    >
      <div
        className="request"
        onClick={openDetails}
        id={ob_id}
        style={{ background: first_color }}
      >
        <div className="request-m">
          <span className="name">{props.request.name}</span>
          <span className="vehicle">{props.request.vehicle}</span>
        </div>
        <span className="time">
          {new Date(props.request.timeStamp).toLocaleTimeString()}
        </span>
      </div>
    </SwipeableListItem>
  );
};

export default Request;
