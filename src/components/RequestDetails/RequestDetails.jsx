import "./RequestDetails.css";
import ReactDOM from "react-dom";
import { useEffect } from "react";

const RequestDetails = (props) => {
  let deatil_id = "deatils-" + props.index;

  const clearPop = () => {
    ReactDOM.render(<div />, document.getElementById("details"));
  };

  return (
    <div className="request-details-back" onClick={clearPop}>
      <div
        className="request-details"
        id={deatil_id}
        style={{ top: props.loc }}
      >
        <div>
          <span className="detail-name">שם: </span>
          <span>{props.request.name}</span>
        </div>
        <div>
          <span className="detail-name">כלי: </span>
          <span>{props.request.vehicle}</span>
        </div>
        <div>
          <span className="detail-name">סיבה: </span>
          <span>{props.request.reason}</span>
        </div>
        <div>
          <span className="detail-name">למתי: </span>
          <span>{props.request.when}</span>
        </div>
        <div>
          <span className="detail-name">לכמה זמן: </span>
          <span>{props.request.howLong}</span>
        </div>
      </div>
    </div>
  );
};

export default RequestDetails;
