import logo from './logo.svg';
import './App.css';
import { useEffect, useState } from 'react';
import Request from "./components/Request/Request"
import { SwipeableList } from '@sandstreamdev/react-swipeable-list';
import '@sandstreamdev/react-swipeable-list/dist/styles.css';

const API_LINK = "https://script.google.com/macros/s/AKfycbyIqUKu-Fr9YgyWHJWxX-lIhLVaCu6C5w-370kKv2PI5nAt6srUf83G/exec"





function App() {
  window.serverUrl = API_LINK
  const [requests, setRequests] = useState(null)

  const load_requests = () => {
    setInterval(() => {
      console.log("sending update request")

      fetch(API_LINK)
      .then(response => response.json())
      .then((data) => {
        console.log("recived update request")
        setRequests(data)
      });
    }, 2000);
    
  }

  const get_requests = () => {

    if (requests) {
      var views = []
      for (const key in requests) {
        const request = requests[key];
        views.push(
          <Request request={request} index={key}></Request>
        )
      }
      if (views.length > 0)
        return views.reverse()
      return (<div>אין בקשות</div>)
    
    }
  }

  useEffect(() => {
    console.log("starting useEffect")
    load_requests()
  }, [])

  return (
    <div className="App">
          <div className="app-header">
            <span>רקמיס'ט</span>
            <img className="head-icon" src="tank.jpg"></img>
          </div>
          <SwipeableList>
            {get_requests()}
          </SwipeableList>
    </div>
  );
}

export default App;
